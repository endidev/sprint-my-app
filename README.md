# Sprint 01 Acamica

API de gestion de pedidos del restaurant Delilah Restó

## Instalacion

Usamos el Administrador de paquetes de Node ([npm](https://www.npmjs.com/)), para instalar el Sprint 01 Acamica.

```bash
npm install
```

## Usamos

```javascript
express -v 4.14.1
swagger-jsdoc -v 6.1.0
swagger-ui-express -v 4.1.6
yamljs -v 0.3.0
nodemon -v 2.0.12
```

## Desarrollo
Creado por Martinez, Hector Adrian.