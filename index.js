const express = require('express');
const YALM = require('yamljs');
const swaggerUI = require('swagger-ui-express');
const swaggerJsDoc = YALM.load('./api.yaml');

const app = express();
const { json } = require('express');
app.use(express.json());
app.use("/api-docs", swaggerUI.serve, swaggerUI.setup(swaggerJsDoc));


let usuarios = [
    {
        id: 1,
        usuario: "g0d",
        nombreApellido: "Adrian Martinez",
        email: "adrian@martinez.com",
        telefono: "+54 2964419635",
        direccion: "Calle Falsa 123",
        password: "0303456",
        admin: true,
        logeado: false
    },
    {
        id: 2,
        usuario: "Regular Human",
        nombreApellido: "Jhon Doe",
        email: "jhon@doe.com",
        telefono: "+54 2964441526",
        direccion: "Calle Verdadera 123",
        password: "0303456",
        admin: false,
        logeado: false
    }
];


let productos = [
    {
        id: 1,
        nombre: "Hamburguesa Clasica",
        precio: 350,
        short: "HamClas"
    },
    {
        id: 2,
        nombre: "Sandwich Veggie",
        precio: 310,
        short: "SanVeg"
    },
    {
        id: 3,
        nombre: "Bagel de Salmon",
        precio: 425,
        short: "BagSal"
    },
    {
        id: 4,
        nombre: "Ensalada Veggie",
        precio: 340,
        short: "EnsVeg"
    },
    {
        id: 5,
        nombre: "Veggie Avocado",
        precio: 310,
        short: "VegAvo"
    },
    {
        id: 6,
        nombre: "Focaccia",
        precio: 300,
        short: "Focaccia"
    },
    {
        id: 7,
        nombre: "Sandwich Focaccia",
        precio: 440,
        short: "SanFoc"
    },
    {
        id: 8,
        nombre: "Sandwich Queso",
        precio: 268,
        short: "SanQueso"
    },
    {
        id: 9,
        nombre: "Hamburguesa Especial",
        precio: 251,
        short: "HamSpeshial"
    },
    
];

let pedidos = [
    {
        id: 1,
        estado: 1,
        hora: "13:30",
        descripcion: "1 x HambClas, 1 x SanVeg",
        pago: 660,
        metodo: 1,
        usuarioId: 1,
        usuarioNombre: 'g0d',  
        direccion: 'Calle Falsa 123',
        pedido: {
            id: 1,
            cantidad: 2
        }
    }
];


let mediosPago = [
    {
        id: 1,
        metodo: "Efectivo" 
    },
    {
        id: 2,
        metodo: "Debito" 
    },
    {
        id: 3,
        metodo: "Transferencia" 
    },
    {
        id: 4,
        metodo: "Credito" 
    }
]

let estadosPedido = [
    {
        id: 1,
        metodo: "Pendiente" 
    },
    {
        id: 2,
        metodo: "Confirmado" 
    },
    {
        id: 3,
        metodo: "En preparacion" 
    },
    {
        id: 4,
        metodo: "Enviado" 
    },
    {
        id: 5,
        metodo: "Entregado" 
    }
]

/**
 * MIDDLEWARE
 */

//Controla que el usuario sea administrador antes de acceder al endpoint.
const esAdmin = (req, res, next) => {
    const  idUser  = req.headers.token;
    
    let found = usuarios.find( x => x.id == parseInt(idUser) && x.admin == true);

    if(found){
        next();
    } else {
        res.status(400).send('El usuario no es administrador.');
    }
};

//Controla que el usuario este logueado antes de acceder al endpoint.
const estaLogueado = (req, res, next) => {
    const  idUser  = req.headers.token;

    let found = usuarios.find( x => x.id == parseInt(idUser) && x.logeado == true);

    if(found){
        next();
    } else {
        res.status(400).send('Usuario no esta logueado.');
    }
};

//controla si existe el producto y que el pedido y el token pertenescan a la misma persona
const pedidoEditable = (req, res, next) => {
    const idProd = parseInt(req.params.id);
    const idUser = parseInt(req.headers.token);
    
    let index = pedidos.findIndex( x => x.id === idProd);
    
    if(index == -1){
        res.status(400).send('Pedido inexistente.');
    } else  {
        if(pedidos[ index ].usuarioId != idUser){
            res.status(400).send('No puede editar el pedido de otro usuario.');
        } else {
            if(pedidos[index].estado == 1){
                next();
            } else {
                res.status(400).send('El pedido esta confirmado (o superior), ya no se puede cambiar.');
            }
        }
    }
};


/**
 * Login y crear usuarios
 */

app.get('/usuarios', (req, res) => {
    //test gitlab
    res.send(usuarios);
})

app.post("/usuarios/crear", (req, res) => {
    
    let usuario = req.body;

    if(usuarios.find( x => x.email == usuario.email)){
        res.status(400).send('Ese mail ya fue utilizado.');
    } else {
        usuario.id = usuarios[usuarios.length - 1].id + 1;
        usuario.admin = false;
        usuario.logeado = false;
        usuarios.push(usuario);
        res.send('Usuario creado con exito.');
    }
});


app.post("/login", (req, res) => {
    const { email,  password } = req.body;

    const found = usuarios.find( x => x.email === email && x.password === password);

    if(found){
        found.logeado = true;
        res.send('Usuario Logueado.');
    } else {
        res.status(400).send('Usuario o Email incorrectos.');
    }
});


/** 
 * PEDIDOS
 */

app.get("/pedidos", [ estaLogueado, esAdmin ], (req, res) => {
    res.send(pedidos);
});


app.post("/pedidos/crear",  estaLogueado , (req, res) => {
    const idUser = parseInt(req.headers.token);
    const { compra, metodo, nuevaDireccion } = req.body;
    
    let direccion;
    if(nuevaDireccion == ""){
        direccion = false;
    } else {
        direccion = nuevaDireccion;
    }

    const pedidoControlado = controlProd( compra );

    if(pedidoControlado){
        if(controlPago(metodo)){
            let d = new Date();
            let hora = `${d.getHours()}:${d.getMinutes()}`;
    
            //Evito otro control, ya que si llego a este punto es un usuario logueado.
            let index = usuarios.findIndex( x => x.id == idUser);
            
            const ordenCompra = {
                    id: pedidos[pedidos.length - 1].id + 1,
                    estado: 1,
                    hora: hora,
                    descripcion: pedidoControlado.descripcion,
                    pago: pedidoControlado.importe,
                    metodo: metodo,
                    usuarioId: idUser,
                    usuarioNombre: usuarios[ index ].usuario,
                    direccion: direccion ? direccion : usuarios[ index ].direccion,
                    pedido: compra
            };

            pedidos.push(ordenCompra); 
            
            res.send('Pedido cargado con exito.');
        } else {
            res.status(400).send('Metodo de Pago incorrecto.');
        }
    } else {
        res.status(400).send('La compra tiene un objeto incorrecto, verifique los datos y envie nuevamente el pedido.');
    }

});


app.get("/pedidos/:id", estaLogueado, (req, res) => {
    const id = parseInt(req.params.id);
    const idUser = parseInt(req.headers.token);

    let filtrado = pedidos.find( x => x.id === id);
    const usuario = usuarios.find( x => x.id === idUser);

    if(filtrado == undefined){
        res.send('No posee pedidos con ese ID.');
    } else {
        
        if(filtrado.usuarioId == usuario.id){
            // Si es la misma persona esta habilitada para ver su propia lista de pedidos
            res.send(filtrado);
        } else {
            // no es la misma persona
            if(usuario.admin){
                //Es admin, puede ver
                res.send(filtrado);
            } else {
                //No es admin, informar el error
                res.status(400).send('Ud no puede ver los pedidos de un tercero.');
            }
        }
    }
       
});


app.put('/pedidos/:id/editarEstado', [ estaLogueado, esAdmin ], (req, res) => {
    
    const { estadoEditar } =  req.body;
    const idProd = parseInt(req.params.id);

    let index = pedidos.findIndex( x => x.id === idProd);
    
    if(index != -1){
        if( controlEstado(estadoEditar) ){
            pedidos[ index ].estado = estadoEditar;    
            res.send("Pedido Actualizado.");
        } else {
            res.status(400).send('El estado no posee un valor aceptable, verifique la documentacion.');
        }
    } else {
        res.status(400).send('El ID del pedido es incorrecto.');
    }
});


app.put('/pedidos/:id/confirmarPedido', [ estaLogueado ], (req, res) => {
    
    const idProd = parseInt(req.params.id);
    const idUser = parseInt(req.headers.token);

    let index = pedidos.findIndex( x => x.id === idProd);
    
    
    if(index != -1){
        if(pedidos[ index ].usuarioId != idUser){
            res.status(400).send('No puede modificar un pedido que no le pertenece.')
        } else {
            pedidos[ index ].estado = 2;
            res.send("Su pedido fue confirmado.");
        }
    } else {
        res.status(400).send('El ID del pedido es incorrecto.');
    }
});


app.put('/pedidos/:id/editar',  [ estaLogueado, pedidoEditable ], (req, res) => {
    const id = parseInt(req.params.id);    
    const { compra, metodo, nuevaDireccion } = req.body;

    let direccion;
    if(nuevaDireccion == ""){
        direccion = false;
    } else {
        direccion = nuevaDireccion;
    }

    let index = pedidos.findIndex( x => x.id === id);

    const pedidoControlado = controlProd( compra );

    if(pedidoControlado){
        if(controlPago(metodo)){
            pedidos[index].descripcion = pedidoControlado.descripcion;
            pedidos[index].pago = pedidoControlado.importe;
            pedidos[index].metodo = metodo;
            pedidos[index].pedido = compra;
            pedidos[index].direccion = direccion ? direccion : pedidos[index].direccion;
            res.send('Pedido actualizado.');    
        } else {
            res.status(400).send('Metodo de pago incorrecto.');
        }
    } else {
        res.status(400).send('La compra tiene un objeto incorrecto, verifique los datos y envie nuevamente el pedido.');
    }
});


/**
 * PRODUCTOS
 */

app.get("/productos", estaLogueado, (req, res) => {
    res.send(productos);
});


app.post("/productos/crear", [ estaLogueado, esAdmin ], (req, res) => {
    
    const { nombre, precio, short } = req.body;

    const prodNuevo = {
        id: productos[productos.length - 1].id + 1,
        nombre,
        precio,
        short
    };

    productos.push(prodNuevo);

    res.send('Producto creado con exito.');
});


app.put('/productos/:id/editar', [ estaLogueado, esAdmin ], (req, res) => {
    const id = req.params.id;
    const { nombre, precio, short} = req.body;

    let index = productos.findIndex( x => x.id === parseInt(id));

    if(index != -1){
        productos[index].nombre = nombre ? nombre : productos[index].nombre;
        productos[index].precio = precio ? precio : productos[index].precio;
        productos[index].short = short ? short : productos[index].short;
        res.send('Producto Editado.');
    } else {
        res.status(400).send('No se puede editar, ID incorrecto.');
    }

});


app.delete('/productos/:id/eliminar', [ estaLogueado, esAdmin ], (req, res) => {
    
    const id = parseInt(req.params.id);

    let index = productos.findIndex( x => x.id === id);
    
    if(index != -1){
        productos.splice(index, 1);
        res.send('Producto Eliminado.');
    } else {
        res.status(400).send('ID de producto incorrecto.');
    }
});


/**
 * Medios de pago
 */
app.get("/mediosPago", estaLogueado, (req, res) => {
    res.send(mediosPago);
});


app.post('/mediosPago/crear', [ estaLogueado, esAdmin ], (req, res) => {
    const { metodo } = req.body;

    const pago = {
        id: mediosPago[mediosPago.length - 1].id + 1,
        metodo
    }

    mediosPago.push(pago);
    res.send('Creado nuevo medio de pago.');
});


app.put('/mediosPago/:id/editar', [ estaLogueado, esAdmin ], (req, res) => {
    const id = parseInt(req.params.id);
    const { metodo } = req.body;

    let medioP = mediosPago.find( x => x.id === id);
    
    if(medioP){
        medioP.metodo = metodo;
        res.send('Metodo de pago actualizado.');
    } else {
        res.status(400).send('ID del medio de pago incorrecto.');
    }
});


app.delete('/mediosPago/:id/eliminar', [ estaLogueado, esAdmin ], (req, res) => {
    
    const id = parseInt(req.params.id);
    
    let index = mediosPago.findIndex( x => x.id === id);
    
    if(index != -1){
        mediosPago.splice(index, 1);
        res.send('Medio de pago Eliminado.');
    } else {
        res.status(400).send('ID del medio de pago incorrecto.');
    }

});


app.listen(3000, () => {
    console.log('Up en 3000');
});



const controlPago = (id) => {
    if(mediosPago.find( x => x.id === parseInt(id))){
        return true;
    } else {
        return false;
    }
};

const controlEstado = (estado) => {
    if(estadosPedido.find( x => x.id === parseInt(estado))){
        return true;
    } else {
        return false;
    }
}

const controlProd = (compra) => {
    
    let descripcion = "";
    let importe = 0;
    let flag = 0;

    compra.forEach(item => {
        if(item.cantidad <= 0){
            return false;
        } else {
            productos.forEach(producto => {
                if(producto.id == item.id){
                    descripcion += `${ producto.short} x ${ item.cantidad } `;
                    importe += ( producto.precio * item.cantidad );
                    flag++;
                }
            });
        }
    });

    if(flag != compra.length){
        return false;
    } else {
        return (compraDetalle = {
            descripcion: descripcion.trim(),
            importe
        })
    }
}

